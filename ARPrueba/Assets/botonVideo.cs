﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class botonVideo : MonoBehaviour, IVirtualButtonEventHandler {
    public GameObject btn;
	// Use this for initialization
	void Start () {
        btn.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
	}

    public void OnButtonPressed(VirtualButtonBehaviour vb) {
        Application.OpenURL("http://www.etitudela.com/profesores/jfcm/mipagina/downloads/manualdelafresadorahaas.pdf");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb) { 
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
