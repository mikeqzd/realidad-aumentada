﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class videovideo : MonoBehaviour, IVirtualButtonEventHandler {
    public UnityEngine.Video.VideoPlayer videoplayer;
    public GameObject Quad;
    public GameObject ReproducirVideo;

	// Use this for initialization
	void Start () {
        Quad = GameObject.Find("Quad");
        Quad.SetActive(false);
        ReproducirVideo.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
	}

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Quad.SetActive(true);
        videoplayer.Play();
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Quad.SetActive(false);
        videoplayer.Pause();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
